import { h, createContext } from 'preact'

export const GlobalCtx = createContext(null)

export default function ({config, getToken, isVerifiedUser, children = null}) {
	return (
		<GlobalCtx.Provider value={{config, getToken, isVerifiedUser}}>
			{children}
		</GlobalCtx.Provider>
	)
}
