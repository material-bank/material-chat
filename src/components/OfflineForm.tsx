import { h } from 'preact'
import { useContext } from 'preact/hooks'
import { css } from '../style'
import ChatBubble from './ChatBubble'
import { GlobalCtx } from './GlobalCtx'

export default function ({ onSubmit, state, setState }) {
	const { offlineMessage } = useContext(GlobalCtx).config;

	return (
		<form onSubmit={onSubmit} className={css`
			display: grid;
			grid-template-rows: 1fr auto;
			overflow: auto;
		`}>
			<div className={css`
				display: grid;
				grid-template-columns: 1fr;
				grid-template-rows: auto auto auto 1fr;
				gap: 1em;
				padding: var(--gutter);
			`}>
				<ChatBubble>{offlineMessage}</ChatBubble>
				<input autoFocus={true} value={state.name} onInput={e => {
					setState(state => ({
						...state,
						name: e.currentTarget.value,
					}))
				}} placeholder='Name (optional)' className='input'/>
				<input required value={state.email} onInput={e => {
					setState(state => ({
						...state,
						email: e.currentTarget.value,
					}))
				}} placeholder='Email' type='email' className='input'/>
				<textarea required value={state.text} onInput={e => {
					setState(state => ({
						...state,
						text: e.currentTarget.value,
					}))
				}} placeholder='How can we help you?' className='textarea'/>
			</div>
			<div className={css`
				padding: var(--gutter);
				border-top: 1px solid rgba(0,0,0,0.1);
			`}>
				<button className={'button '+(state.isBusy ? 'busy' : '')}>Send</button>
			</div>
		</form>
	)
}
