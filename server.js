const http = require('http');
const fs = require('fs').promises;

const requestListener = function (req, res) {
	switch (req.url) {
		case '/': {
			fs.readFile(__dirname + '/public/index.html').then(contents => {
				res.setHeader('Content-Type', 'text/html');
				res.writeHead(200);
				res.end(contents);
			})
			break;
		}
		case '/logo.svg': {
			fs.readFile(__dirname + '/public/logo.svg').then(contents => {
				res.setHeader('Content-Type', 'image/svg+xml');
				res.writeHead(200);
				res.end(contents);
			})
			break;
		}
		case '/dist/livechat.js': {
			fs.readFile(__dirname + '/public/dist/livechat.js').then(contents => {
				res.setHeader('Content-Type', 'application/javascript');
				res.writeHead(200);
				res.end(contents);
			})
			break;
		}
		case '/dist/livechat.js.map': {
			fs.readFile(__dirname + '/public/dist/livechat.js.map').then(contents => {
				res.setHeader('Content-Type', 'application/javascript');
				res.writeHead(200);
				res.end(contents);
			})
			break;
		}
	}
}

const server = http.createServer(requestListener);
port = 80;
server.listen(port, function(){
	console.log(`Listening on ${port}`)
});